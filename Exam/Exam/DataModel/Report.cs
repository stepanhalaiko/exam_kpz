﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Exam.DataModel
{
    public class Report
    {
        
        [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        
        public int Id { get; set; }
        public Car Car { get; set; }
        public int TotalPrice { get; set; }
        [Column(TypeName = "varchar"), MaxLength(255)]
        public string TradeInfo { get; set; }
    }
}
