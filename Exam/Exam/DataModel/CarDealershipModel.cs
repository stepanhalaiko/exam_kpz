using Exam.DataModel;
using System;
using System.Data.Entity;
using System.Linq;


namespace Exam
{
    public class CarDealershipModel : DbContext
    {

        public CarDealershipModel()
            : base("name=CarDealershipModel")
        {
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet <Report> Reports { get; set; }
        
    }

}