﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam.DataModel
{
    public class Car
    {
        
        [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "varchar"), MaxLength(50)]
        public string Model { get; set; }
        [Column(TypeName = "varchar"), MaxLength(20)]
        public string Brand { get; set; }
        public int Price { get; set; }
    }
}
