﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam.DataModel
{
    public enum ManagerRank {
        Junior,
        Middle,
        Senior
    }
    public class Manager
    {
       
         [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "varchar"), MaxLength(50)]
        public string Name { get; set; }
        public ManagerRank Rank { get; set; }
        public int Bonus { get; set; }

    }
}
