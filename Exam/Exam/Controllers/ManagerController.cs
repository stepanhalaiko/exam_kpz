﻿using Exam.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Controllers
{
    public class ManagerController
    {
        public static CarDealershipModel context;
        public static bool CreateManager(string name, ManagerRank rank, int bonus)
        {
            try
            {
                Manager manager = new Manager();
                manager.Rank = rank;
                manager.Name = name;
                manager.Bonus = bonus;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateManager(int Id, string name, ManagerRank rank, int bonus)
        {
            try
            {
                context.Managers.Where(elem => elem.Id == Id).ToList().ForEach(elem =>
                {
                    elem.Name = name;
                    elem.Rank = rank;
                    elem.Bonus = bonus;
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteManager(int Id)
        {
            try
            {
                context.Managers.Remove(context.Managers.Where(elem => elem.Id == Id).ToList()[0]);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
