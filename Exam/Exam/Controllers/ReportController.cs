﻿using Exam.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Controllers
{
    internal class ReportController
    {
        public static CarDealershipModel context;
        public static bool CreateReport(int totalPrice, string tradeInfo, int carId)
        {
            try
            {
                Report report = new Report();
                report.Car = context.Cars.Where(elem => elem.Id == carId).ToList()[0];
                report.TotalPrice = totalPrice;
                report.TradeInfo = tradeInfo;
                context.Reports.Add(report);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateManager(int totalPrice, string tradeInfo, int carId)
        {
            try
            {
                context.Reports.Where(elem => elem.Id == carId).ToList().ForEach(elem =>
                {
                    elem.TotalPrice = totalPrice;
                    elem.TradeInfo = tradeInfo;
                    elem.Car = context.Cars.Where(element => element.Id == carId).ToList()[0];
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteReport(int Id)
        {
            try
            {
                context.Reports.Remove(context.Reports.Where(elem => elem.Id == Id).ToList()[0]);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
