﻿using Exam.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Controllers
{
    public static class CarController
    {
        public static CarDealershipModel context;
        public static bool CreateCar(string Brand, string Model, int Price) {
            try
            {
                Car car = new Car();
                car.Brand = Brand;
                car.Model = Model;
                car.Price = Price;
                context.Cars.Add(car);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateCar(int Id, string Brand, string Model, int Price) {
            try
            {
                context.Cars.Where(elem => elem.Id == Id).ToList().ForEach(elem =>
                {
                    elem.Brand = Brand;
                    elem.Model = Model;
                    elem.Price = Price;
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteCar(int Id) {
            try
            {
                Car carToRemove = context.Cars.Where(elem => elem.Id == Id).ToList()[0];

                context.Managers.ToList().ForEach(elem =>
                {
                    switch (elem.Rank) {
                        case ManagerRank.Junior:
                            elem.Bonus += carToRemove.Price/100;
                            break;
                        case ManagerRank.Middle:
                            elem.Bonus += carToRemove.Price / 80;
                            break;
                        case ManagerRank.Senior:
                            elem.Bonus += carToRemove.Price / 50;
                            break;
                    }
                });

                context.Cars.Remove(carToRemove);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex) {
                return false;
            }
        }
    }
}
