﻿namespace Exam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CarDealership1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "TotalPrice", c => c.Int(nullable: false));
            AddColumn("dbo.Reports", "Car_Id", c => c.Int());
            CreateIndex("dbo.Reports", "Car_Id");
            AddForeignKey("dbo.Reports", "Car_Id", "dbo.Cars", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reports", "Car_Id", "dbo.Cars");
            DropIndex("dbo.Reports", new[] { "Car_Id" });
            DropColumn("dbo.Reports", "Car_Id");
            DropColumn("dbo.Reports", "TotalPrice");
        }
    }
}
