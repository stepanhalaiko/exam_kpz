﻿// <auto-generated />
namespace Exam.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class CarDealership : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CarDealership));
        
        string IMigrationMetadata.Id
        {
            get { return "202212160737234_CarDealership"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
