﻿namespace Exam.Migrations
{
    using Exam.DataModel;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Exam.CarDealershipModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Exam.CarDealershipModel context)
        {
            IList<Car> cars = new List<Car>();

            cars.Add(new Car() { Model = "A6", Brand = "Audi", Price = 22000});
            cars.Add(new Car() { Model = "M6", Brand = "BMW", Price = 30000 });
            cars.Add(new Car() { Model = "Logan", Brand = "Reno", Price = 10000 });

            IList<Manager> managers = new List<Manager>();

            managers.Add(new Manager() { Name = "Erik", Rank = ManagerRank.Middle, Bonus = 0 });
            managers.Add(new Manager() { Name = "Robert", Rank = ManagerRank.Junior, Bonus = 0 });
            managers.Add(new Manager() { Name = "Oksana", Rank = ManagerRank.Senior, Bonus = 10000 });

            context.Cars.AddRange(cars);
            context.Managers.AddRange(managers);

            base.Seed(context);
        }
    }
}
