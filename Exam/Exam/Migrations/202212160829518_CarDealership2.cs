﻿namespace Exam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CarDealership2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Reports", "IdCar");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reports", "IdCar", c => c.Int(nullable: false));
        }
    }
}
