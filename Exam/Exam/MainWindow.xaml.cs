﻿using Exam.Controllers;
using Exam.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static CarDealershipModel context;
        public MainWindow()
        {
            InitializeComponent();
        
            context = new CarDealershipModel();
            CarController.context = context;
            ManagerController.context = context;
            ReportController.context = context;

            dataGridCar.ItemsSource = context.Cars.ToList();
            dataGridManager.ItemsSource = context.Managers.ToList();
            dataGridReport.ItemsSource = context.Reports.ToList();

        }


        private void CarRemove_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem =(Car) dataGridCar.SelectedItem;
            

            if (selectedItem != null)
            {
                ReportController.CreateReport(selectedItem.Price, "Selled", selectedItem.Id);
                CarController.DeleteCar(selectedItem.Id);
                dataGridCar.ItemsSource = context.Cars.ToList();
                dataGridReport.ItemsSource = context.Reports.ToList();
            }
        }

        private void ManagerRemove_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (Manager)dataGridManager.SelectedItem;


            if (selectedItem != null)
            {
                ManagerController.DeleteManager(selectedItem.Id);
                dataGridManager.ItemsSource = context.Managers.ToList();
            }
        }
        private void ReportRemove_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (Report)dataGridReport.SelectedItem;


            if (selectedItem != null)
            {
                ReportController.DeleteReport(selectedItem.Id);
                dataGridReport.ItemsSource = context.Reports.ToList();
            }
        }

        private void dataGridCar_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = false;
            var item = e.Row.Item as Car;
            if (item != null)
            {
                CarController.UpdateCar(item.Id, item.Brand,item.Model, item.Price);
            }
        }

        private void CarAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!(BrandInput.Text == "" && ModelInput.Text == "" && PriceInput.Text == "")) {
                try
                {
                    CarController.CreateCar(BrandInput.Text, ModelInput.Text, int.Parse(PriceInput.Text));
                    dataGridCar.ItemsSource = context.Cars.ToList();
                }
                catch {
                    PriceInput.Text = "Enter valid number";
                }
            }
        }

        private void CarUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!(IdInput.Text==""&&BrandInput.Text == "" && ModelInput.Text == "" && PriceInput.Text == ""))
            {
                try
                {
                    CarController.UpdateCar(int.Parse(IdInput.Text),BrandInput.Text, ModelInput.Text, int.Parse(PriceInput.Text));
                    dataGridCar.ItemsSource = context.Cars.ToList();
                }
                catch
                {
                    PriceInput.Text = "Enter valid number";
                }
            }
        }
    }
}
